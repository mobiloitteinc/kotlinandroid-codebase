package com.samplecode.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.samplecode.R
import com.samplecode.databinding.ActivityLoginBinding
import com.samplecode.viewmodel.LoginViewModel


class LoginActivity : AppCompatActivity() {

    var binding: ActivityLoginBinding? = null
    var viewmodel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewmodel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding?.viewmodel = viewmodel
        initObservables()


    }



    private fun initObservables() {
//        viewmodel?.progressDialog?.observe(this, Observer {
//            if (it!!) customeProgressDialog?.show() else customeProgressDialog?.dismiss()
//        })

        viewmodel?.userLogin?.observe(this, Observer { user ->
            Toast.makeText(this, "welcome, ${user?.username}", Toast.LENGTH_LONG).show()
        })
    }


}