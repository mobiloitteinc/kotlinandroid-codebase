package com.samplecode.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.widget.Toast
import com.samplecode.model.User
import com.samplecode.utils.CommonUtils
import com.samplecode.utils.SingleLiveEvent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    private val ctx: Context? = application
    var btnSelected: ObservableBoolean? = null
    var email: ObservableField<String>? = null
    var password: ObservableField<String>? = null
    var userLogin: MutableLiveData<User>? = null


    init {
        btnSelected = ObservableBoolean(false)
        email = ObservableField("")
        password = ObservableField("")
        userLogin = MutableLiveData<User>()
    }

    fun onEmailChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        btnSelected?.set(CommonUtils.isEmailValid(s.toString()) && password?.get()!!.length >= 8)


    }

    fun onPasswordChanged(s: CharSequence, start: Int, befor: Int, count: Int) {
        btnSelected?.set(CommonUtils.isEmailValid(email?.get()!!) && s.toString().length >= 8)

    }

    fun login(){
        showMessage(this!!.ctx!!, "Hello World")
//        val show: Any = Toast.makeText(this@LoginViewModel, "Hello World", Toast.LENGTH_SHORT).show()
    }
    fun showMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }




}